using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace WorkerService
{
    public class Worker : BackgroundService
    {
        private readonly ILogger<Worker> _logger;

        public Worker(ILogger<Worker> logger)
        {
            _logger = logger;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            await DoWork(stoppingToken);
        }

        private async Task DoWork(CancellationToken stoppingToken)
        {
            stoppingToken.ThrowIfCancellationRequested();

            var factory = new ConnectionFactory() { HostName = "localhost" };
            var connection = factory.CreateConnection();
            var channel = connection.CreateModel();
            
            channel.QueueDeclare(queue: "hello",
                                durable: false,
                                exclusive: false,
                                autoDelete: false,
                                arguments: null);

            ExecuteConsumer(channel, "consumer a");
            ExecuteConsumer(channel, "consumer b");

            await Task.CompletedTask;
        }


        public void ExecuteConsumer(IModel channel, string consumerName)
        {
            channel.BasicQos(0, 4, false);

            var consumer = new EventingBasicConsumer(channel);
            consumer.Received += (model, ea) =>
            {
                try
                {
                    var body = ea.Body.ToArray();
                    var message = Encoding.UTF8.GetString(body);

                    Console.WriteLine($"{consumerName}: {message}");
                    _logger.LogInformation($"{consumerName}: {message}");

                    if (message == "Later")
                    {
                        DoLater();
                    }

                    channel.BasicAck(ea.DeliveryTag, false);
                }
                catch (Exception e)
                {
                    channel.BasicNack(ea.DeliveryTag, false, true);
                    Console.WriteLine("Error: {0}", e.Message);
                }

            };

            channel.BasicConsume(queue: "hello",
                                 autoAck: false,
                                 consumer: consumer);
        }


        public static void DoLater()
        {
            var factory = new ConnectionFactory() { HostName = "localhost" };
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                IDictionary<string, object> args = new Dictionary<string, object>
                {
                    {"x-delayed-type", "direct"}
                };
                channel.ExchangeDeclare("DelayedTest", "x-delayed-message", true, false, args);

                var queue = channel.QueueDeclare("DelayedMessageQueue", true, false, false, null);
                channel.QueueBind(queue, "DelayedTest", "dtest");

                var props = channel.CreateBasicProperties();
                props.Headers = new Dictionary<string, object>
                {
                    {"x-delay", 60 * 60 * 1000}
                };

                var now = DateTimeOffset.Now;

                channel.BasicPublish("DelayedTest", "dtest", props, Encoding.Default.GetBytes($"This message has been sending at {now.ToString("HH:mm:ss")}"));
            }
        }
    }
}
